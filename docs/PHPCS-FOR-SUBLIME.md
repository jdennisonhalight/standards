# Installing PHPCS for Sublime Text

1 Install [SublimeLinter](https://github.com/SublimeLinter/SublimeLinter)

2 Install [SublimeLinter-phpcs](https://github.com/SublimeLinter/SublimeLinter-phpcs)

3 Open User Preferences for SublimeLinter (Preferences > Package Settings > SublimeLinter > Settings). Make sure to edit the "User" preferences, not the default settings.

4 Update / add `phpcs` object to `linters` object, setting the `standard` to the mapped network drive path to `phpcs.xml`:

```json
{
    "linters": {
        "phpcs": {
            "@disable": false,
            "standard": "Z:\\conduit-standards\\phpcs.xml"
        },
    }
}
```

5 Under the `paths` object, add `conduit-standards/vendor/bin` to windows:

```json
{
    "paths": {
        "windows": [
            "Z:\\conduit-standards\\vendor\\bin"
        ]
    }
}
```

The complete file will look something like this (omitting options not related to phpcs)

```json
{
    "user": {
        "linters": {
            "phpcs": {
                "@disable": false,
                "standard": "Z:\\conduit-standards\\phpcs.xml"
            },
        },
        "paths": {
            "windows": [
                "Z:\\conduit-standards\\vendor\\bin"
            ]
        }
    }
}
```

## Testing

To confirm phpcs is working, create a new php file with the following code:

```php
<?php
class ClassName extends AnotherClass
{

    function __construct(argument)
    {
        # code...
    }
}
```

You should have ~11 errors.

## Troubleshooting

In sublime, open the console with `ctrl+~` (backtick) to see any console errors reported by SublimeLinter.

> SublimeLinter: reason: [WinError 5] Access is denied

`phpcs.bat` is not executable. Run:

```bash
chmod +x /opt/www/conduit-standards/vendor/bin/phpcs.bat
```

(This is already done in install script)
