# `pre-commit`

## pre-commit-00-phpcs

Runs [PHPCS](https://github.com/squizlabs/PHP_CodeSniffer) on staged files that are about to be committed. If phpcs finds any errors, it will abort the commit, and show a list of errors you must fix.

## pre-commit-01-twig-lint

Runs [twig lint](https://github.com/asm89/twig-lint) on staged twig files that are about to be committed. If twig lint finds any errors, it will abort the commit, and show a list of errors you must fix.
