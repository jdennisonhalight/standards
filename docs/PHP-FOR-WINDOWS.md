# Installing PHP for Windows

1. Install the latest version of PHP, Non Thread Safe, from [http://windows.php.net/download/](http://windows.php.net/download/)
1. Create `C:\php`, and extract downloaded contents there.
1. Update your Windows path to include `C:\php`. (see below for instructions)
1. Verify PHP is installed by running `php --version` in a command prompt. (win+r, cmd)

## Adding PHP to your Windows path with Rapid Environment Editor

1. [Download Rapid Environment Editor](https://www.rapidee.com/en/download)
1. Open it with administrator privileges (find in start, right click, run as administrator)
1. Under **System Variables** on the left, right click **Path** and click **Add Value**
1. Enter path where you extracted php: `C:\php`
1. Hit save button at the top.
1. Verify PHP is installed by running `php --version` in a command prompt. (win+r, cmd)

**NOTE:** This PHP version is unrelated to the version you run in your conduit vm.
