# `commit-msg`

## commit-msg-00-git-good-commit

Runs [git-good-commit](https://github.com/tommarshall/git-good-commit) to check if your commit follows some basic rules.

## commit-msg-01-jira-ticket-num

If the current branch is a JIRA ticket branch, this will auto prepend the ticket number to each commit message.
