# Installing PHPCS for Atom

1. Read [PHP for Windows](PHP-FOR-WINDOWS.md) to install PHP on your Windows machine.
1. Install [linter-phpcs](https://github.com/AtomLinter/linter-phpcs) for atom.
1. In the settings, set the following:

**Executed Path:**
```
\\192.168.86.40\www\conduit-standards\vendor\bin\phpcs.bat
```

Update `192.168.86.40` to your vm's IP address.

Set this value to conduit-standards phpcs.bat, via your virtual machines IP through windows. You may also use a mapped network drive instead (Z:\conduit-standards, etc)

**Search for Executables**: UNCHECK

**Code Standard or Config File:**

```
\\192.168.86.40\www\conduit-standards\phpcs.xml
```

This also must point to your virtual machine through windows.

**Disable When No Config File**: CHECK

**Search for configuration files**: UNCHECK

These settings will force Atom to use phpcs executable / ruleset file from conduit-standards, instead of the ones from your local project directory.

## Testing

To confirm phpcs is working, create a new php file with the following code:

```php
<?php
class ClassName extends AnotherClass
{

    function __construct(argument)
    {
        # code...
    }
}
```

You should have ~11 errors.
