# Conduit Standards

## Purpose

This repo contains configuration files / standards that should be used for all conduit repos.

## Install

Simply run `./install.sh` from the terminal! 😁

The install script will install [PHPCS](https://github.com/squizlabs/PHP_CodeSniffer), and run a few other commands, including:

* Creating a symbolic link `/opt/www/.editorconfig` -> `/opt/www/conduit-standards/.editorconfig` - Your editor should pick up this file automatically if there is not already a .editorconfig in the project.

* Creating a symbolic link `/opt/www/phpcs.xml` -> `/opt/www/conduit-standards/phpcs.xml` - Your editor should pick up this file automatically if there is not already a phpcs.xml in the project.

* Setting the global hooks folder to the local `git-hooks` folder

* Creating symlinks for the current git hooks, to the `hook-chain` script. This allows multiple git hooks to be run.

### Disabling git hooks on a per repo basis

You can run this command inside your repo to reset git hooks back to `.git/hooks` dir:

```bash
git config core.hookspath $GIT_DIR/hooks
```

**NOTE:** This should only be used for legacy apps that don't, and will never follow the current standards.

## Using PHPCS in your editor

The pre-commit hook in this repo runs PHPCS against your staged files, but you can also see the errors in your editor beforehand.

Please refer to the docs for installing PHPCS for your editor:

* [Atom](docs/PHPCS-FOR-ATOM.md)
* [Sublime Text](docs/PHPCS-FOR-SUBLIME.md)

### Git Extensions / Using Git on Windows

If you are using Git Extensions on Windows, please follow these **one-time** instructions:

1. Open Git Extensions.
1. Click Tools > Git Bash.

Run:
```bash
git config --global core.hooksPath './../conduit-standards/git-hooks'
```

This tells git on your windows machine to use the git-hooks folder in this repo. This is already done in the composer install for your virtual machine.

To **undo** this, run this:

```bash
git config --global --unset core.hooksPath
```

## Git Hooks

See the docs for each git hook:

* [`pre-commit`](docs/PRE-COMMIT.md)

* [`commit-msg`](docs/COMMIT-MSG.md)

**NOTE:** You can skip all git hooks by running `git commit --no-verify.` (Only use when necessary)
