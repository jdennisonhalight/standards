#!/bin/bash

composer install

# Create symbolic links for config files
echo -e "\nCreating config file symlinks...\n"
ln --symbolic --force --verbose "/opt/www/conduit-standards/.editorconfig" "/opt/www/.editorconfig"
ln --symbolic --force --verbose "/opt/www/conduit-standards/phpcs.xml" "/opt/www/phpcs.xml"

# Setup hook chains
echo -e "\nSetting up hook chains...\n"
ln --symbolic --force --verbose "/opt/www/conduit-standards/git-hooks/hook-chain" "/opt/www/conduit-standards/git-hooks/commit-msg"
ln --symbolic --force --verbose "/opt/www/conduit-standards/git-hooks/hook-chain" "/opt/www/conduit-standards/git-hooks/pre-commit"

# Set the hooks path to the git-hooks here, so all repos will use those hooks
git config --global core.hooksPath "./../conduit-standards/git-hooks"

# Set phpcs.bat to be executable for sublime text
chmod +x /opt/www/conduit-standards/vendor/bin/phpcs.bat

echo -e "\nDone.\n"
